const { Storage } = require('@google-cloud/storage');
const { PubSub } = require('@google-cloud/pubsub');
const ffmpeg = require('fluent-ffmpeg');
const fs = require('fs');
const winston = require('winston');

// Configuración de Winston para el sistema de logging
const logger = winston.createLogger({
  level: process.env.LOG_LEVEL || 'info',
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.printf(info => `${info.timestamp} - ${info.level}: ${info.message}`)
  ),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: './logs/video_joiner.log' })
  ]
});
// Crear una instancia del cliente de Google Cloud Storage
const storageGcp = new Storage({
  projectId: 'sdpp2-405215',
  keyFilename: '/etc/secret-volume/sdpp2-405215-a21bf250e80c.json',
});
const bucketName = 'videos-sobel';
const framesTopicName = 'frames_processed_topic'; 

const pubsub = new PubSub({
  projectId: 'sdpp2-405215',
  keyFilename: '/etc/secret-volume/sdpp2-405215-a21bf250e80c.json',
});
// Creates a new subscription
const subscriptionName = 'frames_processed_topic-sub'; // Nombre de la suscripción

async function createSubscription() {
  try {
    const [subscription] = await pubsub.topic(framesTopicName).createSubscription(subscriptionName);
    logger.info(`Subscription ${subscription.name} created.`);
  } catch (error) {
    logger.error('Error al crear la suscripción:', error);
  }
}

// Función para unir frames en un video
async function joinFrames(frames, videoId, totalFrames) {
  if (frames.length !== totalFrames) {
    return;
  }

  frames.sort((a, b) => a.frameId - b.frameId);
  const frameFilenames = frames.map((frame, index) => {
    const filename = `/tmp/frame_${videoId}_${index}.jpg`;
    fs.writeFileSync(filename, frame.frameData);
    return filename;
  });

  const outputFilename = `/tmp/${videoId}.mp4`;

  try {
    await new Promise((resolve, reject) => {
      ffmpeg()
        .input(`concat:${frameFilenames.join('|')}`)
        .inputFormat('image2')
        .outputOptions('-c:v libx264')
        .on('end', () => resolve())
        .on('error', (err) => reject(err))
        .output(outputFilename)
        .run();
    });

    // Subir el video generado a Google Cloud Storage
    await storageGcp.bucket(bucketName).upload(outputFilename, {
      destination: `processed/${videoId}.mp4`,
    });

    // Eliminar los archivos temporales
    frameFilenames.forEach((filename) => fs.unlinkSync(filename));
    fs.unlinkSync(outputFilename);

    logger.info(`Video ${videoId} unido y subido a GCS.`);
  } catch (error) {
    logger.error('Error al unir los frames y subir el video:', error);
  }
}

createSubscription();

const messageHandler = async (message) => {
  try {
    const messageData = JSON.parse(message.data.toString('utf8'));
    const { videoId, frameId, totalFrames, frameData } = messageData;

    logger.info(`Received frame ${frameId} for video ${videoId}`);

    if (!VideoJoiner.frames) {
      VideoJoiner.frames = {};
    }

    if (!VideoJoiner.frames[videoId]) {
      VideoJoiner.frames[videoId] = [];
    }

    VideoJoiner.frames[videoId].push({ frameId, frameData });

    // Verificar si se deben unir los frames
    if (VideoJoiner.frames[videoId].length === totalFrames) {
      await joinFrames(VideoJoiner.frames[videoId], videoId, totalFrames);
      delete VideoJoiner.frames[videoId];
    }

    message.ack();
  } catch (error) {
    logger.error('Error al procesar el mensaje:', error);
    message.nack();
  }
};
//obtenemos la sub creada
const subscription = pubsub.subscription(subscriptionName);
subscription.on('message', messageHandler);

